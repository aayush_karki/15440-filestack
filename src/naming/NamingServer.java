package naming;


import java.io.*;

import rmi.*;
import common.*;
import storage.*;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;


/** Naming server.

    <p>
    Each instance of the filesystem is centered on a single naming server. The
    naming server maintains the filesystem directory tree. It does not store any
    file data - this is done by separate storage servers. The primary purpose of
    the naming server is to map each file name (path) to the storage server
    which hosts the file's contents.

    <p>
    The naming server provides two interfaces, <code>Service</code> and
    <code>Registration</code>, which are accessible through RMI. Storage servers
    use the <code>Registration</code> interface to inform the naming server of
    their existence. Clients use the <code>Service</code> interface to perform
    most filesystem operations. The documentation accompanying these interfaces
    provides details on the methods supported.

    <p>
    Stubs for accessing the naming server must typically be created by directly
    specifying the remote network address. To make this possible, the client and
    registration interfaces are available at well-known ports defined in
    <code>NamingStubs</code>.
 */
public class NamingServer implements Service, Registration 
{
	private Skeleton<Service> skeleton_serv;
	private Skeleton<Registration> skeleton_reg;
	
	HashMap<Path, Storage_Command> NS_Files; //NamingServer fileSystem files
	HashSet <Path> NS_Directories;		//NamingServer fileSystem directories
	
	HashMap<Storage_Command, HashSet<Path>> storageServers;
	private int nextStorageServer_idx = 0;
	
	private int STATUS;
	/*There are three states
	 * -the server is up and ready to receive registrations
	 * -the server has registered at least one storage server and is doing
	 *  its Naming tasks
	 *  -the server has been stopped explicitly, and it won't restart after this*/
	private static final int READY = 0;
	private static final int ACTIVE = 1;
	private static final int STOPPED = 2;
	
	
    /** Creates the naming server object.

        <p>
        The naming server is not started.
     */
    public NamingServer()
    {
    	 
        skeleton_serv = new Skeleton<Service>(Service.class, this,
        		new InetSocketAddress(NamingStubs.SERVICE_PORT));
        
        skeleton_reg = new Skeleton<Registration>(Registration.class, this,
        		new InetSocketAddress(NamingStubs.REGISTRATION_PORT));
        
        NS_Files= new HashMap<Path, Storage_Command>();
        NS_Directories = new HashSet<Path>();
        NS_Directories.add(new Path());// the FS of the NS should always have a root.
        storageServers = new HashMap<Storage_Command, HashSet<Path>>();
        
        STATUS = READY;
        
    }

    /** Starts the naming server.

        <p>
        After this method is called, it is possible to access the client and
        registration interfaces of the naming server remotely.

        @throws RMIException If either of the two skeletons, for the client or
                             registration server interfaces, could not be
                             started. The user should not attempt to start the
                             server again if an exception occurs.
     */
    public synchronized void start() throws RMIException
    {
        if (STATUS == ACTIVE){
        	return ;
        }
        else if (STATUS == READY){
        	try {
				skeleton_serv.start();
				skeleton_reg.start();
				STATUS = ACTIVE;
			} catch (RMIException e) {
				throw e;
			}
        	catch(Exception e){
        		e.printStackTrace();;
        	}
        }
        else if(STATUS == STOPPED){
        	throw new  UnsupportedOperationException("Attempting" 
        			+" to start a stopped Naming Server"
        			+ "This is not allowed");
        	
        }
        else{
        	throw new Error("Invalid value of STATUS detected.");
        }
    }
    
 

    /** Stops the naming server.

        <p>
        This method waits for both the client and registration interface
        skeletons to stop. It attempts to interrupt as many of the threads that
        are executing naming server code as possible. After this method is
        called, the naming server is no longer accessible remotely. The naming
        server should not be restarted.
     */
    public void stop()
    {
        if(STATUS==READY || STATUS==STOPPED) return;

			skeleton_serv.stop();
			skeleton_reg.stop();
			stopped(null);			

    }

    /** Indicates that the server has completely shut down.

        <p>
        This method should be overridden for error reporting and application
        exit purposes. The default implementation does nothing.

        @param cause The cause for the shutdown, or <code>null</code> if the
                     shutdown was by explicit user request.
     */
    protected void stopped(Throwable cause)
    {
    		STATUS = STOPPED;
    		
    }

    // The following methods are documented in Service.java.
    @Override
    public boolean isDirectory(Path path) throws FileNotFoundException
    {
    	
    	if (path==null) throw new NullPointerException("Path is null");
    	if (!exists(path)) throw new FileNotFoundException("path doesnt exist"
    			+ "in naming server's file system");
    	
    	return NS_Directories.contains(path);

    }
    
    @Override
    public String[] list(Path directory) throws FileNotFoundException
    {
           	 
    	if (directory==null) throw new NullPointerException("Given dir is null");
    	
    	if (!isDirectory(directory)) throw new FileNotFoundException("Given dir is not in FS");
    	
    	ArrayList<String> children = new ArrayList<String>();
    	
    	String dirStr = directory.toString();
    
    	for (Path file: NS_Files.keySet()){
    		if(!file.isRoot()){
    			if(dirStr.equals(file.parent().toString())){
    				children.add(file.last());
    			}
    		}
    	}
    	
    	
    	for (Path dir: NS_Directories){
    		if(!dir.isRoot()){
    			String parent = dir.parent().toString();
    			if (dirStr.equals(parent)){
    				children.add(dir.last());
    			}
    		}
    	}//for
    	

    	String[] children_str = new String[children.size()];
    	for(int i=0; i<children.size(); i++){
    		children_str[i] = children.get(i);
    	}
    	
    	return children_str;
    	
    }
    
   

    @Override
    public boolean createFile(Path file)
        throws RMIException, FileNotFoundException
    {
        if (file==null) throw new NullPointerException("Null path given.");
        
        if(!file.isRoot() && !NS_Directories.contains(file.parent())){
        	throw new FileNotFoundException("Parent dir of the file"
        			+ "doesn't exist");
        }
        
        if(exists(file)) return false;
        
        Storage_Command tuple = getStorageServer();
        if(tuple.getCommand().create(file)){
        	buildAllSubdirectories(file);
        	NS_Files.put(file,tuple);
        	storageServers.get(tuple).add(file);
        	return true;
        }
        else{
        	return false;
        }
    }
   
    
    @Override
    public boolean createDirectory(Path directory) throws FileNotFoundException
    {
        if (directory==null) throw new NullPointerException("Null directory given.");
        
        if(directory.isRoot() || exists(directory)){
        	return false;
        }
        
        if(!isDirectory(directory.parent())){
        	throw new FileNotFoundException("the parent of the given dir"
        			+ "doesn't exist in the Naming FS");
        }
        
        return NS_Directories.add(directory);
    }

    @Override//i added RMIException
    public boolean delete(Path path) throws FileNotFoundException, RMIException
    {
        if (path==null) throw new NullPointerException("Given path is null");
        
        if (path.isRoot()) return false;
        
        if (!exists(path)) throw new FileNotFoundException("path doesnt exist in the file system");
                
       
        if (!isDirectory(path)){
        	//delete path from the storage before from Naming as deleting might raise RMI Exp
        	//so we want to avoid path being deleted from NS but not from the storage
        	Storage_Command server = NS_Files.get(path);
        	server.getCommand().delete(path);
        	
        	return (NS_Files.remove(path)!=null) 
        			&& 
        			storageServers.get(server).remove(path);
        }
        
        /*remove an empty dir*/
        if (list(path).length == 0){
        	//TODO something might be needed to be added here. What if there is an empty dir in storage server??
        	//Can there even ever be an empty directory in a storage server since the storage server prunes when
        	//we issue command to it to delete a file?
        	//We dont have Storage_Command tuple for a directory. we only have for files. 
        	//Bottomline: does the problem i'm thinking of even exists?
        	
        	return NS_Directories.remove(path);
        }
        /*remove a non-empty dir*/
        else{
        	//all the files in the directories have to be deleted before deleting the subdirectories i think.
        	
        	for (Path f : NS_Files.keySet ()) {
                if (f.parent ().equals (path)) {
                    delete(f);
                }
        	} 
        	
        	for (Path f : NS_Directories  ){
        		if (f.parent().equals(path)){
        			delete(f);
        		}
        	}
    	
        }
        return true;
        
    }

    @Override
    public Storage getStorage(Path file) throws FileNotFoundException
    {
    	if (file ==null) throw new NullPointerException("Given file is null");
    	
    	Storage_Command server_tuple = NS_Files.get(file);
    	if (server_tuple ==null) throw new FileNotFoundException(
    			"Path file not found in the naming server FS"
    			);
    	return server_tuple.getStorage();
    }

    // The method register is documented in Registration.java.
    @Override
    public Path[] register(Storage client_stub, Command command_stub,
                           Path[] files)
    {
    	
    	if (client_stub==null || command_stub==null ||files==null)
    		throw new NullPointerException(
    				"client_stub or command_stub or files is null");

    	Storage_Command tuple = new Storage_Command(client_stub,
    												command_stub);
    	if(storageServers.keySet().contains(tuple) ){
    		throw new IllegalStateException("the storage server"
    				+ "has already been registered. Can't re-register");
    	}
        
    	storageServers.put(tuple, new HashSet<Path>());
    	ArrayList<Path> duplicatePaths = new ArrayList<Path>();
    	
    	// @param 'files' contains all files, no directories.
    	for(Path path: files){
    		if(path.isRoot()){
    			;//do nothing
    		}
    		else if(exists(path)){
    			duplicatePaths.add(path);
    		}
    		else{
    			NS_Files.put(path, tuple);
    			buildAllSubdirectories(path);
    			storageServers.get(tuple).add(path);
    		}
    	}
    	
    	Path[] duplicates = new Path[duplicatePaths.size()];
    	for (int i=0; i<duplicatePaths.size(); i++){
    		duplicates[i] = duplicatePaths.get(i);
    	}
    	
    	return duplicates;
    }
    
    /*
     * Return true if and only if path is a file or a directory
     * that is already registered in this NamingServer
     * */    
    private boolean exists(Path path){
    	if (NS_Directories.contains(path)) return true;
    	if (NS_Files.containsKey(path)) return true;
    	
    	return false;
    }
    

    
//*********************************************************************
    /*
     * @param path - path is non-null and is not a root.
     * */   
    private void buildAllSubdirectories(Path path){
    	Path parent = path.parent();
    	while (!parent.isRoot()){
    		NS_Directories.add(parent);
    		parent = parent.parent();
    	}
    }
    
    /*
     * Returns a storage and command tuple server. 
     * Conceptualizes the list of servers being stored in a circular chain.
     * Every server is returned once in a cycle.
     * */
    private Storage_Command getStorageServer ()
    {
   	
        int idx = nextStorageServer_idx;
        int count = storageServers.size();
        
        if (nextStorageServer_idx < count-1){
        	nextStorageServer_idx++;
        }
        else{
        	nextStorageServer_idx = (nextStorageServer_idx+1)%count;
        }
     
        return (Storage_Command)storageServers.keySet().toArray()[idx];
       
    }
    
    private class Storage_Command{
    	private Storage storage=null;
    	private Command command=null;
    	
		public Storage_Command(Storage storage, Command command) {
			this.storage = storage;
			this.command = command;
		}

		public Storage getStorage() {
			return storage;
		}

		public Command getCommand() {
			return command;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((command == null) ? 0 : command.hashCode());
			result = prime * result + ((storage == null) ? 0 : storage.hashCode());
			return result;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Storage_Command other = (Storage_Command) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (command == null) {
				if (other.command != null)
					return false;
			} else if (!command.equals(other.command))
				return false;
			if (storage == null) {
				if (other.storage != null)
					return false;
			} else if (!storage.equals(other.storage))
				return false;
			return true;
		}

		private NamingServer getOuterType() {
			return NamingServer.this;
		}

    }
    

}