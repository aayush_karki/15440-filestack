package rmi;

 
import java.lang.reflect.*;

/** RMI exceptions. */
public class RMIException extends Exception
{
    /** Creates an <code>RMIException</code> with the given message string. */
    public RMIException(String message)
    {
        super(message);
    }

    /** Creates an <code>RMIException</code> with a message string and the given
        cause. */
    public RMIException(String message, Throwable cause)
    {
        super(message, cause);
    }

    /** Creates an <code>RMIException</code> from the given cause. */
    public RMIException(Throwable cause)
    {
        super(cause);
    }



   /*Anything below this line has been added by me.*/

    public static boolean allMethodsThrowRMIExp(Class<?> c){

        Method[] methods = c.getDeclaredMethods();
        for (Method method : methods ) {
            Class<?>[] exceptions = method.getExceptionTypes();

            boolean throwsRMIException = false;
            //check if RMIException is one of the methods thrown
            //by a method in the interface
            for (Class<?> exception: exceptions ) {
                throwsRMIException = exception.equals(RMIException.class);
                if (throwsRMIException) break;
            }
            if (!throwsRMIException) return false; //A method doesnt throw RMIException
        }

        return true; //all methods throw RMIException
    }

    
    
    
}//RMIException class ends