/**
 * 
 */
package rmi;

import java.io.*;
import java.lang.reflect.*;
import java.lang.reflect.Proxy;
import java.net.*;

/**
 * @author AayushKarki
 *
 *This class is an implementation of a handler for proxy instance in Stub
 */



public class StubHandler implements InvocationHandler, Serializable {
    
    private InetSocketAddress remoteAddress;
    
    
    /** Assigns an address to the class
     * @param address
     */
    public StubHandler(InetSocketAddress address) {
        this.remoteAddress = address;
    }

    /**
     * @return remote address of this class
     */
    private InetSocketAddress getAddress_StubHandler(){
        return this.remoteAddress;
    } 
 

 
    /** Handles remote invocatoins. Passses the methods and variables to
     * the remote skeleton
     * @param proxy
     * @param method
     * @param args
     * @return Object - computed value or an exception
     * @throws Throwable
     */
    private Object invoke_remote(Object proxy, Method method, Object[] args) throws Throwable {
        
        Socket skeletonSock   = new Socket(); 
        boolean gotExceptionFromRemote  ;
        Object result_obj      = null;
        
        try {
            // connect to remote server
        	skeletonSock = new Socket(); 
        	skeletonSock.connect(remoteAddress);
        	
            ObjectInputStream in = new ObjectInputStream(skeletonSock.getInputStream());    
            ObjectOutputStream out = new ObjectOutputStream(skeletonSock.getOutputStream());
            out.flush();
            

            //forward remote call to a 
            out.writeObject(method.getName());
            out.writeObject(method.getParameterTypes());
            out.writeObject(args);
            
            // unmarshall results from remote call
            gotExceptionFromRemote  = (boolean) in.readObject();
            result_obj     = in.readObject();
            
        } catch (Exception e) {
            throw new RMIException(e);
        } finally {
            if(skeletonSock != null && !skeletonSock.isClosed()) {
            	skeletonSock.close();
            }
        }
        
        if(gotExceptionFromRemote == false) {
        	return result_obj;
        }
        else{
        	throw (Throwable) result_obj;
        }
}
    
    
 
    /** Handles local invocations w/o forwarding methods and arguments
     * to a remote skeleton
     * @param proxy
     * @param method
     * @param args
     * @return Object - computed value or an exception
     * @throws Throwable
     */
    private Object invoke_local(Object proxy, Method method, Object[] args)
   		 throws Throwable {
   		         /* There can be three local methods namely- equals(), hash() and toString()
   		         */
	         if(method.getName().equals("hashCode")){
		             StubHandler sh = (StubHandler) Proxy.getInvocationHandler(proxy);
		             int hashcode1 =  sh.getAddress_StubHandler().hashCode();
		             int hashcode2 = proxy.getClass().hashCode();
		             return hashcode1 *1117 + hashcode2;
		         }

		         else if(method.getName().equals("equals")){
		   	        Object proxy2 = args[0];			   	        
		   	        if(proxy2 == null) return false;			   	        
		   	        			   	        
		   	        // compare proxy classes
		   	        if(!Proxy.isProxyClass(proxy2.getClass())) return false;
		   	        
		   	        // class type 
		   	        if (!proxy.getClass().equals(proxy.getClass()))  return false;
		   	        
		   	        InvocationHandler handler = Proxy.getInvocationHandler(proxy2);
		   	        //both proxies should be instanace of same handler class
		   	        if(!handler.getClass().equals(StubHandler.class )  ) return false;
		   	        
		   	        // remote addresses should be same
		   	        if(!remoteAddress.equals(((StubHandler) handler).getAddress_StubHandler())) 
		   	        	return false;
		   	        
		   	        return true;
		          }
		         else{//includes other local methods like notify, notifyAll, toString etc.
		        	 StubHandler sh = (StubHandler) Proxy.getInvocationHandler(proxy);
		        	 return method.invoke(sh, args);
		         }

   		 }
    
    
    /* (non-Javadoc)
     * @see java.lang.reflect.InvocationHandler
     * #invoke(java.lang.Object, java.lang.reflect.Method, java.lang.Object[])
     */
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable{
        //check if method is RMI method
        Class<?>[] exps = method.getExceptionTypes();
        boolean isRMIMethod = false;

        for (Class<?> e: exps ) {
            if (e.equals(RMIException.class)){
                isRMIMethod = true;
            }
        }

        if (isRMIMethod) 
        	 return invoke_remote(proxy, method, args);
        else 
            return invoke_local(proxy, method, args);
    }
    
}